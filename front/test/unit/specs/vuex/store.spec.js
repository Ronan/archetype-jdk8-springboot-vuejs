import { expect } from 'chai'
import { mutations } from 'src/vuex/store'

const { SET_LABELS } = mutations

describe('store', () => {
  it('SET_LABELS should change labels', () => {
    const state = { labels: {} }
    SET_LABELS(state, {about: {version: 'version'}})
    expect(state.labels).to.deep.equal({about: {version: 'version'}})
  })
})
