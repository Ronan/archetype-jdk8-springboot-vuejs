const labels = {
  error: 'Error',
  login: {
    info: 'Please fill up your login and password.',
    error: 'Login and/or password are incorrect.',
    user: 'Login',
    password: 'Password',
    submit: 'Submit'
  },
  about: {
    contact: 'ronan@leroy.com',
    version: '0.0.1-SNAPSHOT'
  }
}

const login = {
  ok: { auth: 'OK' },
  ko: { auth: 'KO' }
}

export const data = {
  labels: labels,
  login: login
}
