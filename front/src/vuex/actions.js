import { fetch, post } from 'src/libs/fetch'

export const loadLabels = ({dispatch}) => {
  fetch('/api/labels',
    json => {
      dispatch('SET_LABELS', json)
    },
    error => console.log(error)
  )
}

export const login = ({dispatch}, msg, success, error) => {
  post('/api/login',
    JSON.parse(msg),
    json => {
      if (json.auth === 'OK') {
        dispatch('SET_LOGIN', true)
        success()
      } else {
        dispatch('SET_LOGIN', false)
        error()
      }
    },
    error => console.log(error)
  )
}
