export function isLogged (state) {
  return state.logged
}

export function getLabels (state) {
  return state.labels
}
