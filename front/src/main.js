import Vue from 'vue'
import VueRouter from 'vue-router'

import PageHome from './components/home/page-home'
import PageLogin from './components/login/page-login'

Vue.use(VueRouter)
var router = new VueRouter()
router.map({
  '/': {
    name: 'home',
    component: PageHome
  },
  '/login': {
    name: 'login',
    component: PageLogin
  }
})

import App from './App'
router.start(App, 'body')
